CC = g++
CFLAGS = -fopenmp -Wall -std=c++11

task_1: task_1.cpp
	$(CC) $(CFLAGS) -o $@ $<

task_2: task_2.cpp
	$(CC) $(CFLAGS) -o $@ $<

all: task_1 task_2

clean:
	rm -f task_1 task_2
	
