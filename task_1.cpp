#include <cstdlib>
#include <cstdio>
#include <chrono>

const int DEFAULT_NUM_RUNS = 10000000;
const int NUM_WORK_ITERATIONS = 0;	// Extra work iterations used for testing.

/**
 * Return a random number in range [low, high].
 * @param low Lowest possible value.
 * @param high Highest possible value.
 */
float randRange(float low, float high)
{
	float range = high - low;
	float scaleValue = range / RAND_MAX;
	return (rand() * scaleValue) + low;
}

float monteCarloPiSingleThread(const int numRuns)
{
	int hits = 0;

	for (int i = 0; i < numRuns; i++) {
		for (int j = 0; j < NUM_WORK_ITERATIONS; j++) {}
		
		float randX = randRange(0.0f, 1.0f);
		float randY = randRange(0.0f, 1.0f);
		if ((randX * randX) + (randY * randY) <= 1.0f) {
			hits += 1;
		}
	}

	float pi = ((float)hits / (float)numRuns) * 4.0f;
	return pi;
}

float monteCarloPiOmpReduction(const int numRuns)
{
	int hits = 0;

	#pragma omp parallel for reduction (+:hits)
	for (int i = 0; i < numRuns; i++) {
		for (int j = 0; j < NUM_WORK_ITERATIONS; j++) {}
		
		float randX = randRange(0.0f, 1.0f);
		float randY = randRange(0.0f, 1.0f);
		if ((randX * randX) + (randY * randY) <= 1.0f) {
			hits += 1;
		}
	}

	float pi = ((float)hits / (float)numRuns) * 4.0f;
	return pi;
}

float monteCarloPiOmpCritical(const int numRuns)
{
	int hits = 0;

	#pragma omp parallel for
	for (int i = 0; i < numRuns; i++) {
		for (int j = 0; j < NUM_WORK_ITERATIONS; j++) {}
		
		float randX = randRange(0.0f, 1.0f);
		float randY = randRange(0.0f, 1.0f);
		if ((randX * randX) + (randY * randY) <= 1.0f) {
			#pragma omp critical (add_sample)
			{
				hits += 1;
			}
		}
	}

	float pi = ((float)hits / (float)numRuns) * 4.0f;
	return pi;
}

float monteCarloPiOmpAtomic(const int numRuns)
{
	int hits = 0;

	#pragma omp parallel for
	for (int i = 0; i < numRuns; i++) {
		for (int j = 0; j < NUM_WORK_ITERATIONS; j++) {}
		
		float randX = randRange(0.0f, 1.0f);
		float randY = randRange(0.0f, 1.0f);
		if ((randX * randX) + (randY * randY) <= 1.0f) {
			#pragma omp atomic
			hits += 1;
		}
	}

	float pi = ((float)hits / (float)numRuns) * 4.0f;
	return pi;
}

int main(int argc, char* argv[])
{
	srand(time(NULL));

	int numRuns = DEFAULT_NUM_RUNS;
	if (argc >= 2) {
		numRuns = atoi(argv[1]);
	}

	printf("Running single thread...\n");
	auto startTime = std::chrono::high_resolution_clock::now();
	float pi = monteCarloPiSingleThread(numRuns);
	auto endTime = std::chrono::high_resolution_clock::now();
	float runTime = (std::chrono::duration<float>(endTime - startTime)).count();
	printf("Single thread got %.4f in %.4fs.\n", pi, runTime);
	
	printf("\nRunning with reduction...\n");
	startTime = std::chrono::high_resolution_clock::now();
	pi = monteCarloPiOmpReduction(numRuns);
	endTime = std::chrono::high_resolution_clock::now();
	runTime = (std::chrono::duration<float>(endTime - startTime)).count();
	printf("OMP with reduction got %.4f in %.4fs.\n", pi, runTime);

	printf("\nRunning with critical...\n");
	startTime = std::chrono::high_resolution_clock::now();
	pi = monteCarloPiOmpCritical(numRuns);
	endTime = std::chrono::high_resolution_clock::now();
	runTime = (std::chrono::duration<float>(endTime - startTime)).count();
	printf("OMP with critical section got %.4f in %.4fs.\n", pi, runTime);

	printf("\nRunning with atomic...\n");
	startTime = std::chrono::high_resolution_clock::now();
	pi = monteCarloPiOmpAtomic(numRuns);
	endTime = std::chrono::high_resolution_clock::now();
	runTime = (std::chrono::duration<float>(endTime - startTime)).count();
	printf("OMP with atomic got %.4f in %.4fs.\n", pi, runTime);
}
