#include <fstream>
#include <thread>
#include <chrono>
#include <string>
#include <cstdio>
#include <omp.h>

void doWork()
{
	std::this_thread::sleep_for(std::chrono::milliseconds(50));
}

std::chrono::duration<float> runTask(const int numThreads, const int percentParallel)
{
	omp_set_num_threads(numThreads);

	auto startTime = std::chrono::high_resolution_clock::now();
			
	// Serial work.
	for (int i = percentParallel; i < 100; i++) {
		doWork();
	}
			
	// Parallel work.
	#pragma omp parallel for
	for (int i = 0; i < percentParallel; i++) {
		doWork();	
	}

	auto endTime = std::chrono::high_resolution_clock::now();

	return std::chrono::duration<float>(endTime - startTime);
}

int main(int argc, char* argv[])
{
	std::string csvString = "";
	int lastNumThreads = 1024;
	int percentParallelStep = 20;
	int percentParallelFirst = 10;
	int percentParallelLast = 90;

	// Add headers to each column of CSV.
	for (int numThreads = 1; numThreads <= lastNumThreads; numThreads *= 2) {
		csvString += ", " + std::to_string(numThreads);
	}
	csvString += "\n";

	for (int percentParallel = percentParallelFirst; percentParallel <= percentParallelLast; percentParallel += percentParallelStep) {
		csvString += std::to_string(percentParallel) + "% paralell, ";
		
		auto singleThreadRunDuration = runTask(1, percentParallel);
		float singleThreadRunTime = singleThreadRunDuration.count();
		
		printf("%i%% parallel on 1 thread took %.4fs.\n", percentParallel, singleThreadRunTime);
		csvString += "1.0";
		
		for (int numThreads = 2; numThreads <= lastNumThreads; numThreads *= 2) {
			auto runDuration = runTask(numThreads, percentParallel);
			float runTime = runDuration.count();

			float speedup = singleThreadRunTime / runTime;

			printf("%i%% parallel on %i threads took %.4fs with a %0.4fx speedup.\n", percentParallel, numThreads, runTime, speedup);
			csvString += ", " + std::to_string(speedup);
		}

		csvString += "\n";
	}

	std::ofstream csvFile("timings.csv");
	csvFile << csvString;
}
